sofia-sip (1.12.1.dfsg-1) unstable; urgency=low

  * New upstream version

  [ George Danchev ]
  * Repackage original tarball as dfsg, several rfc*.txt removed
  * Added print-version, get-orig-source targets to rules
  * Packages renamed -- sofia-sip-bin (executables inside)
    libsofia-sip-ua0, libsofia-sip-ua-glib0 as suggested by libpkg-guide#5
  * Version Build-Depends for debhelper level4 as suggested by cdbs docs
  * Added watch file and TODO.Debian
  * Adjusted maintainer and uploaders fields
  * Split-off a sofia-sip-doc package
  * Build-Depends on graphviz since /ust/bin/dot is needed at build time
  * Do not override cdbs targets/vars -- common things should stay common
  * Install all man pages (sip-dig.1, stunc.1 have been added with that upstream released)
  * Fix old malformed changelog entries -- keeps lintian happy
  * Updated to Standards-Version: 3.7.2
  * Remove RFC-licensed sha1 copyright, since it is dropped upstream as of 1.12.1
  * Initial release (Closes: #373173 -- the ITP for sofia-sip)

  [ Kai Vehmanen ]
  * Updated the package descriptions, and the notice concerning
    shared-library naming and version.

 -- George Danchev <danchev@spnet.net>  Sat, 29 Jun 2006 12:32:14 +0300

sofia-sip (1.11.8-1) unstable; urgency=high

  * Updated to 1.11.8 upstream version.

 -- Kai Vehmanen <kai.vehmanen@nokia.com>  Mon, 22 May 2006 21:14:48 +0300

sofia-sip (1.11.6-2) unstable; urgency=high

  * Missing *.so in -dev package fixed.

 -- Philippe Kalaf <mail@unknown.dom>  Fri, 03 Mar 2006 11:14:12 +0200

sofia-sip (1.11.6-1) unstable; urgency=high

  * Updated to 1.11.6 upstream version.

  * Added missing .install files for libsofaia-sipua and
    libsofia-sip-ua-dev.

 -- Kai Vehmanen <kai.vehmanen@nokia.com>  Thu, 02 Mar 2006 15:54:01 +0200

sofia-sip (1.11.5-2) unstable; urgency=low

  * Applied changes from Dmitry Rozhkov. Checked packages with
    lintian.

 -- Kai Vehmanen <kai.vehmanen@nokia.com>  Thu, 09 Feb 2006 15:05:46 +0200

sofia-sip (1.11.5-1) unstable; urgency=low

  * Updated to the new upstream release. Changed to use cdbs for rules. Divided into separate library and tools packages.

 -- Kai Vehmanen <kai.vehmanen@nokia.com>  Wed, 08 Feb 2006 21:03:50 +0200

sofia-sip (1.11.2) unstable; urgency=low

  * SDP offer-answer supported in SOA API

 -- Martti Mela <martti.mela@nokia.com>  Mon, 31 Oct 2005 15:25:23 +0200

sofia-sip (1.11.1) unstable; urgency=low

  * This version never existed as as arm deb package

 -- Martti Mela <martti.mela@nokia.com>  Mon, 31 Oct 2005 15:24:49 +0200

sofia-sip (1.11.0) unstable; urgency=low

  * Initial Release.

 -- Martti Mela <martti.mela@nokia.com>  Tue, 12 Oct 2005 09:54:40 +0300
